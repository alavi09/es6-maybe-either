/**
 * Either
 */

class Either {
    constructor(val) {
        this.value = val
    }

    static Left(val) {
        return new Left(val)
    }

    static Right(val) {
        return new Right(val)
    }

    // --- Applicative ---

    static of (val) {
            return new Right(val)
        }

        of (val) {
            return new Right(val)
        }

}

/**
 * Left subclass
 */

class Left extends Either {
    constructor(val) {
        super(val)
    }

    chain(f) {
        return new Left(this.value) 
    }

    map(f) {
        return new Left(this.value) 
    }

    fold(f, _) {
        return f(this.value) 
    }

}

/**
 * Right subclass
 */

class Right extends Either {
    constructor(val) {
        super(val)
    }

    chain(f) {
        return f(this.value)
    }

    map(f) {
        return this.of(f(this.value))
    }

    fold(_, g) {
        return g(this.value)
    }

}

module.exports = Either;


