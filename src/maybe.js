/**
 * Maybe
 */
class Maybe {
    constructor(val){
        this.value = val
    }

    /**
     * 
     * @param {any} val - takes in some kinda value(number, string, null etc)
     * 
     * lift method; returns a Maybe given any kind of value.
     * 
     */
    static of (val) {
        return new Maybe(val)
    }

    /**
     *  Check if value inside Maybe is either null or Undefined
     *  Returns true or false
     */    
    isNothing () {
        return (Object.is(this.value, null) || Object.is(this.value, undefined))
    }

    /**
     * 
     * @param {function} fn - A callback function
     *
     * map returns a new Maybe object after performing the callback function on
     * the value inside the maybe 
     * 
     * If the Maybe holds a value that is a null or Undefined then a Maybe with a 
     * value of null is returned
     */
    map (fn) {
        return this.isNothing() ? Maybe.of(null) : Maybe.of(fn(this.value));
    }
}

module.exports = Maybe;


