const getType = function(value) {
    return Object.prototype.toString.call(value).slice(8, -1)
}

module.exports = getType;