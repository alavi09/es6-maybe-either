const Either = require('./either')
const log = require('./log')
const getType = require('./getType')
const Maybe = require('./maybe')

// Basic use of Either
log(Either.Right(3).map(x => x+1).fold(e => "Error", n => n+1)) //5
log(Either.Left(3).map(x => x+1).fold(e => "Error", n => n+1)) //Error

// Either sample usage
const findNum = (num) => {
    let found = false;
    for (let x of [1, 2, 3]) {
        if (x === num) {
            found = true
        }
    }
    // if a value is found return a Right or else if not found return a Left
    return found ? Either.Right(found) : Either.Left(found);
}

log(findNum(1).map(x => x.toString()).map(x => x.toUpperCase()).fold(e => "no num", n => n)) // TRUE
log(findNum(5).map(x => x.toString()).map(x => x.toUpperCase()).fold(e => "no num", n => n)) // no num